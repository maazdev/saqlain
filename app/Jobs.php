<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services;


class Jobs extends Model
{
    //
    protected $table='jobs';

    protected $fillable=[
        'service_id','job_by','job_to','status_id','rating','review'
    ];


    public function ServiceJobs(){
        return $this->belongsTo('App\Services','service_id','id');
    }

    public function StatusJobs(){
        return $this->belongsTo('App\Status','status_id','id');
    }
    public function JobByModel(){
        return $this->belongsTo('App\User','job_by','id');
    }
    public function JobToModel(){
        return $this->belongsTo('App\User','job_to','id');
    }

}
