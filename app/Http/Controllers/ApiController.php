<?php

namespace App\Http\Controllers;

use App\Http\Middleware\UserMiddleware;
use App\Http\Middleware\VendorMiddleware;
use App\Jobs;
use App\Services;
use App\ServiceVendors;
use App\Role;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Symfony\Component\EventDispatcher\Tests\Service;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Tests\DependencyInjection\RemoveEmptyControllerArgumentLocatorsPassTest;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

class ApiController extends Controller
{
    //
    public function Test(){
        return response()->json(['success'=>true,'message'=>'Do It']);
    }
    public function getCities(){
        $cities=User::select('city')->distinct('city')->where('role_id',2)->get();
        return response()->json(['success'=>true,'cities'=>$cities]);

    }

    public function getPendingJobs(){
        $vendor_id=Auth::user()->id;
        $jobs=Jobs::where('job_to',$vendor_id)->where('status_id',1)->with('ServiceJobs')->with('StatusJobs')->with('JobByModel')->with('JobToModel')->get();
        return response()->json(['success'=>true,'jobs'=>$jobs]);

    }

    public function getOnGoingJobs(){
        $vendor_id=Auth::user()->id;
        $jobs=Jobs::where('job_to',$vendor_id)->where('status_id',2)->with('ServiceJobs')->with('StatusJobs')->with('JobByModel')->with('JobToModel')->get();
        return response()->json(['success'=>true,'jobs'=>$jobs]);

    }
    public function getDoneJobs(){
        $vendor_id=Auth::user()->id;
        $jobs=Jobs::where('job_to',$vendor_id)->where('status_id',3)->with('ServiceJobs')->with('StatusJobs')->with('JobByModel')->with('JobToModel')->get();
        return response()->json(['success'=>true,'jobs'=>$jobs]);

    }

    public function getAllJobs(){
        $vendor_id=Auth::user()->id;
        $jobs=Jobs::where('job_to',$vendor_id)->with('ServiceJobs')->with('StatusJobs')->with('JobByModel')->with('JobToModel')->get();
        return response()->json(['success'=>true,'jobs'=>$jobs]);

    }
    public function getStatus(){
        $status=Status::get();
        return response()->json(['success'=>true,'status'=>$status]);
    }

    public function getVendors(){
        $vendors=ServiceVendors::with('ServicePivot')->with('UserPivot')->get();
        return response()->json(['success'=>true,'vendors'=>$vendors]);
    }

    public function getAllServices(){
        $services=Services::get();
        return response()->json(['success'=>true,'services'=>$services]);
    }
    public function registerService(Request $request){
        $vendor_id=Auth::user()->id;
        if($request->has('service_id')&&$request['service_id']!=''){
            $serviceVendor=ServiceVendors::create([
                'service_id'=>$request['service_id'],
                'user_id'=>$vendor_id
            ]);
            return response()->json(['success'=>true,'servicevendor'=>$serviceVendor]);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Select A Valid Service ID']);
        }
    }

    public function getRoles(){
        $roles=Role::get();
        return response()->json(['success'=>true,'roles'=>$roles]);

    }


    //New Rules

    public function SelectJob(Request $request){
        $user_id=Auth::user()->id;
        //$job_id=$request['job_id'];
        $service_id=$request['service_id'];
        $job_by=$user_id;
        $job_to=$request['job_to'];
        $job=Jobs::create([
            'service_id'=>$service_id,
            'job_by'=>$job_by,
            'job_to'=>$job_to,
            'status_id'=>1,

        ]);
        return response()->json(['success'=>true,'message'=>'Job Sent To Vendor Successfully']);
    }


    public function AcceptJob(Request $request){
        $job=Jobs::where('id',$request['job_id'])->first();
        $job->status_id=2;
        $job->save();
        return response()->json(['success'=>true,'message'=>'Job Started Successfully']);
    }

    public function CompleteJob(Request $request){
        $job=Jobs::where('id',$request['job_id'])->first();
        $job->status_id=3;
        $job->save();
        return response()->json(['success'=>true,'message'=>'Job Completed Successfully']);
    }

    public function RateReview(Request $request){
        $job=Jobs::where('id',$request['job_id'])->first();
        $job->rating=$request['rating'];
        $job->review=$request['review'];
        $job->save();
        return response()->json(['success'=>true,'message'=>'Review Added Successfully']);
    }

    public function getUserCompletedJobs(){
        $user_id=Auth::user()->id;
        $jobs=Jobs::where('job_by',$user_id)->with('ServiceJobs')->with('StatusJobs')->with('JobByModel')->with('JobToModel')->get();
        return response()->json(['success'=>true,'jobs'=>$jobs]);
    }



}
