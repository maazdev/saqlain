<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceVendors extends Model
{
    //
    protected $table='service_vendors';

    protected $fillable=['service_id','user_id'];

    public function ServicePivot(){
        return $this->belongsTo('App\Services','service_id','id');
    }

    public function UserPivot(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
