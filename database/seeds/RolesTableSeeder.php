<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::create([
            'role_name'=>'User'
        ]);

        Role::create([
        'role_name'=>'Vendor'
        ]);

        Role::create([
            'role_name'=>'Admin'
        ]);

    }
}
