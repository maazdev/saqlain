<?php

use Illuminate\Database\Seeder;
use App\Services;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Services::create([
            'service_name'=>'Plumber'
        ]);
        Services::create([
            'service_name'=>'Electrician'
        ]);
        Services::create([
            'service_name'=>'Mechancic'
        ]);
    }
}
