<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });


    $api->group(['middleware' => ['auth:api', 'user']], function(Router $api) {
        // You will write here your endpoints.
        $api->get('done','App\\Http\\Controllers\\ApiController@Test');
        $api->get('vendors','App\\Http\\Controllers\\ApiController@getVendors');
        $api->post('select/job','App\\Http\\Controllers\\ApiController@SelectJob');
        $api->get('completed/jobs','App\\Http\\Controllers\\ApiController@getUserCompletedJobs');
        $api->post('rate/job','App\\Http\\Controllers\\ApiController@RateReview');
    });

    $api->group(['middleware' => ['auth:api', 'vendor']], function(Router $api) {
        $api->post('add/service','App\\Http\\Controllers\\ApiController@registerService');
        $api->get('pending/jobs','App\\Http\\Controllers\\ApiController@getPendingJobs');
        $api->get('ongoing/jobs','App\\Http\\Controllers\\ApiController@getOnGoingJobs');
        $api->get('done/jobs','App\\Http\\Controllers\\ApiController@getDoneJobs');
        $api->get('all/jobs','App\\Http\\Controllers\\ApiController@getAllJobs');
        $api->post('accept/job','App\\Http\\Controllers\\ApiController@AcceptJob');
        $api->post('complete/job','App\\Http\\Controllers\\ApiController@CompleteJob');
        // You will write here your endpoints.
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
    $api->get('cities','App\\Http\\Controllers\\ApiController@getCities');
    $api->get('status','App\\Http\\Controllers\\ApiController@getStatus');
    $api->get('services','App\\Http\\Controllers\\ApiController@getAllServices');
    $api->get('roles','App\\Http\\Controllers\\ApiController@getRoles');

});
